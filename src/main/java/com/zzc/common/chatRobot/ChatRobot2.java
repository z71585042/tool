package com.zzc.common.chatRobot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 聊天机器人，调用聊天接口
 * 使用HttpURLConnection 实现http请求
 * @author Administrator
 *
 */
public class ChatRobot2 implements Callable<String> {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String url = "http://www.xiaohuangji.com/ajax.php";
	private String msg;
	
	public ChatRobot2(String msg){
		this.msg = msg;
	}
	
	@Override
	public String call(){
		String retStr = null;
		URL url = null;
		BufferedReader in = null;
		try {
//			url = new URL(this.url+"?para="+this.msg);
			//创建链接
			url = new URL(this.url);
			HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
			httpURLConnection.setRequestMethod("POST");
			
			//设置参数并发送请求
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setReadTimeout(4500);//4.5秒超时
			String postData = "para="+URLEncoder.encode(this.msg,"UTF-8");//要传出的参数
			httpURLConnection.getOutputStream().write(postData.getBytes());
			httpURLConnection.getOutputStream().flush();
			httpURLConnection.getOutputStream().close();
			
			//处理请求
			int responseCode = httpURLConnection.getResponseCode();
			logger.debug("----->http response code is "+responseCode);
			if(200==responseCode){
				in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			    String inline = "";  
			    StringBuilder str = new StringBuilder();
			    while ((inline =in.readLine()) != null){
			    	str.append(inline);
			    }
			    retStr = str.toString();
			    logger.debug("----->response msg is "+retStr);
			}else{
				logger.error("http response code is "+responseCode);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}finally{
			try {
				if(in!=null){
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return retStr;
		
	}

}
