package com.zzc.common.chatRobot;

import java.io.IOException;
import java.util.concurrent.Callable;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 聊天机器人，调用聊天接口
 * 使用commons-httpclient实现
 * openshift不支持使用httpclient发送请求，没办法，此类被ChatRobot2代替
 * @author Administrator
 *
 */
public class ChatRobot implements Callable<String> {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String msg;
	private static HttpClient httpClient;//客户端
	
	static{
		httpClient = new HttpClient();
		
//		HostConfiguration hostConfiguration = new HostConfiguration();
		//修改端口，因为openshift目前只允许使用 between 15000 and 35530 port
//		Protocol protocol = new Protocol("http", new DefaultProtocolSocketFactory(), 15888);
//		hostConfiguration.setHost("127.0.0.1", 15888,protocol);
//		httpClient.setHostConfiguration(hostConfiguration);
		httpClient.getParams().setParameter(HttpMethodParams.HTTP_URI_CHARSET, "UTF-8");//请求参数编码格式
		httpClient.getParams().setContentCharset("UTF-8");//内容编码格式
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(4500);//超时时间设置为4.5秒
	}
	
	public ChatRobot(String msg){
		this.msg = msg;
	}
	
	@Override
	public String call() throws Exception {
		String retStr = null;
		
		PostMethod method = new PostMethod("http://www.xiaohuangji.com/ajax.php");//构造请求
		method.setRequestBody(new NameValuePair[]{//设置请求参数
			new NameValuePair("para",this.msg)
		});
		
//		执行请求
		try {
			int httpStatus = httpClient.executeMethod(method);
			if(httpStatus == HttpStatus.SC_OK){
				logger.debug("httpStatue ok:"+method.getResponseBodyAsString());
				retStr = method.getResponseBodyAsString();
			}else{
				logger.error("httpStatue error:"+httpStatus+"|"+retStr);
			}
		} catch (HttpException e) {
			logger.error(e.getMessage(),e);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
		return retStr;
	}

}
