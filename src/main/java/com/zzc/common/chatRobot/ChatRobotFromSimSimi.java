package com.zzc.common.chatRobot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 聊天机器人，调用聊天接口
 * 这个是调用simsimi官方网站的聊天应用
 * 使用HttpURLConnection 实现http请求
 * @author Administrator
 *
 */
public class ChatRobotFromSimSimi implements Callable<String> {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String url = "http://www.simsimi.com/func/req";
	private String msg;
//	private String cookie = "Filtering=1.0; __utma=119922954.863574391.1388714996.1389246822.1389611463.7; __utmz=119922954.1388726708.3.3.utmcsr=baidu|utmccn=(organic)|utmcmd=organic|utmctr=simsimi; selected_nc=ch; __utmb=119922954.3.9.1389611485552; __utmc=119922954; popupCookie=true";
	
	public ChatRobotFromSimSimi(String msg){
		this.msg = msg;
	}
	
	@Override
	public String call(){
		String retStr = null;
		URL url = null;
		BufferedReader in = null;
		try {
			url = new URL(this.url+"?msg="+this.msg+"&ft=1.0&lc=ch");
			//创建链接
//			url = new URL(this.url);
			HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
			
//			httpURLConnection.setRequestProperty("Connection", "keep-alive");
//			httpURLConnection.setRequestProperty("Cookie", this.cookie);
			httpURLConnection.setRequestProperty("Referer", "http://www.simsimi.com/talk.htm");
//			httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0");
//			httpURLConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			
			httpURLConnection.setRequestMethod("GET");
			
			//设置参数并发送请求
//			httpURLConnection.setDoOutput(true);
			httpURLConnection.setReadTimeout(4500);//4.5秒超时
//			String postData = "msg="+URLDecoder.decode(this.msg,"UTF-8")+"&ft=1.0&lc=ch";
//			httpURLConnection.getOutputStream().write(postData.getBytes());
//			httpURLConnection.getOutputStream().flush();
//			httpURLConnection.getOutputStream().close();
			
			//处理请求
			int responseCode = httpURLConnection.getResponseCode();
			logger.debug("----->http response code is "+responseCode);
			if(200==responseCode){
				in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			    String inline = "";  
			    StringBuilder str = new StringBuilder();
			    while ((inline =in.readLine()) != null){
			    	str.append(inline);
			    }
			    String rspStr = str.toString();//url返回结果
			    
			    logger.debug("----->response msg is "+rspStr);
			    
			    //转化为json对象
			    JSONObject json = JSONObject.fromObject(rspStr);
			    retStr = json.optString("response");
			    
			}else{
				logger.error("http response code is "+responseCode);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}finally{
			try {
				if(in!=null){
					in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return retStr;
		
	}

}
