package com.zzc.common;

/**
 * 
 * @author zhengzhichao
 *
 */
public class CommonConstants {
	
	/**
	 * 校验重复段子的时间范围 单位为天
	 */
	public static final int CHECK_REPEAT_SCOPE = 7;
	/**
	 * 相似度，以此来判断两个段子是否相同
	 */
	public static final float SIMILAR_VALUE = (float) 0.7; 
	
	/**
	 * 系统配置
	 * @author zhengzhichao
	 *
	 */
	public enum WebConfigKey{
		/**
		 * 过滤关键字，匹配的字符串将被过滤掉，使用空字符串进行替换
		 * 1.使用|分割
		 */
		FILTER_KEY_WORD,
		/**
		 * 校验的关键字，匹配的段子将直接被忽略 
		 * 1.使用|分割每个关键字 
		 * 2.出现% 执行like匹配
		 */
		CHECK_KEY_WORD;
	}
	
	/**
	 * 序列名称
	 * @author zhengzhichao
	 *
	 */
	public enum SequenceName{
		JOKE_ID("joke_id");
		private String value;
		private SequenceName(String value) {
			this.value = value;
		}
		public String getValue() {
			return value;
		}
	}
	
	/**
	 * 段子来源
	 * @author zhengzhichao
	 *
	 */
	public enum JokeSourceType{
		QB,//糗事百科
		NH,//内涵段子
		BDJ;//不的姐
	}
	
	/**
	 * 段子的状态
	 * @author zhengzhichao
	 *
	 */
	public enum JokeStatus{
		NORMAL,//正常
		INACTIVE,//未激活
		DEL;//删除
	}
}