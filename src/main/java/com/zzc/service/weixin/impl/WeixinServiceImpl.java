package com.zzc.service.weixin.impl;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.zzc.common.chatRobot.ChatRobotFromSimSimi;
import com.zzc.service.weixin.WeixinService;

/**
 * 微信服务
 * @author zhengzhichao
 *
 */
@Service("weixinService")
public class WeixinServiceImpl implements WeixinService{
	
	private final Logger logger = LoggerFactory.getLogger(WeixinServiceImpl.class);
	private ExecutorService executorService = Executors.newCachedThreadPool();//线程池

	private String accessToken;//微信access_token
	private long expireTime;//超时时间，这里记录的是一个超时的时间点,毫秒表示
	
	//以下为微信提供的
	private String grantType;
	private String appid;
	private String secret;
	
	@Override
	public String chat(String message) {
		String ret = null;
		Future<String> future = executorService.submit(new ChatRobotFromSimSimi(message));//发送异步请求
		try {
			ret = future.get();
		} catch (InterruptedException e) {
			logger.error(e.getMessage(),e);
		} catch (ExecutionException e) {
			logger.error(e.getMessage(),e);
		}
		return ret!=null?ret:"/::< 服务器爆炸了，请稍等....";
	}

	@Override
	public void initAccessToken() {
		
	}
}
