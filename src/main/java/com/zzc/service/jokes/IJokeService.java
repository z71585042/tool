package com.zzc.service.jokes;

import java.util.List;

import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.db.mongo.morphia.base.entity.PagingResult;

public interface IJokeService {
	
	/**
	 * 校验插入
	 * @param jokeEntity
	 * @return
	 */
	public Boolean checkAndAddJoke(JokeEntity jokeEntity) throws Exception;
	
	/**
	 * 
	 * @param id id标示位
	 * @param page 当前第几页
	 * @param size 当前页面大小
	 * @return
	 * @throws Exception
	 */
	public PagingResult<JokeEntity> queryJokeByPaging(String id,int page,int size);
	
	/**
	 * 查询jokes
	 * @param s 标识位 s=0表示获取最新的jokes 否则表示增量加载
	 * @param size 加载数量
	 * @return
	 */
	public List<JokeEntity> queryJokesForApp(String s,int size);
	
	/**
	 * 根据content，判断是否出现重复内容的段子
	 * 1.关于判断重复的范围，如果范围太大导致效率降低
	 * 2.如何判断重复，目前使用编辑距离算法计算相似度
	 * @param content
	 * @return
	 */
	public Boolean checkContentRepeat(String content);
	
	/**
	 * 根据content，进行关键字匹配
	 * @param content
	 * @return
	 */
	public Boolean checkContentKeyWord(String content);
	/**
	 * 过滤关键字，所有关键字将被剔除
	 * 返回处理过的文本
	 * @param content
	 * @return
	 */
	public String filterContent(String content);
	
	/**
	 * 顶几次
	 * @param jokeId
	 * @param count
	 */
	public void up(String jokeId,int count);
	
	/**
	 * down几次次
	 * @param jokeId
	 * @param count
	 */
	public void down(String jokeId,int count);
}
