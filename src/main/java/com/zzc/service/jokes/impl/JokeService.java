package com.zzc.service.jokes.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.jmkgreen.morphia.query.Query;
import com.github.jmkgreen.morphia.query.UpdateOperations;
import com.zzc.common.CommonConstants;
import com.zzc.common.CommonConstants.JokeSourceType;
import com.zzc.common.CommonConstants.JokeStatus;
import com.zzc.common.CommonConstants.SequenceName;
import com.zzc.common.CommonConstants.WebConfigKey;
import com.zzc.common.tool.SequenceHelp;
import com.zzc.common.tool.StringUtil;
import com.zzc.db.mongo.dao.IJokeDao;
import com.zzc.db.mongo.dao.IWebConfigDao;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.db.mongo.entity.WebConfigEntity;
import com.zzc.db.mongo.morphia.base.entity.Paging;
import com.zzc.db.mongo.morphia.base.entity.PagingResult;
import com.zzc.service.jokes.IJokeService;

/**
 * @author zhengzhichao
 *
 */
@Service
public class JokeService implements IJokeService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Resource
	private IJokeDao jokeDao;
	@Resource
	private IWebConfigDao webConfigDao;
	
	@Override
	public Boolean checkAndAddJoke(JokeEntity jokeEntity) throws Exception{
		
		String sourceId = jokeEntity.getSourceId();
		JokeSourceType sourceType = jokeEntity.getSourceType();
		
		if(sourceId == null || sourceId.length() == 0){
			throw new Exception("sourceId 不能为空");
		}
		
		if(sourceType == null){
			throw new Exception("sourceType 不能为空");
		}
		
		if(jokeEntity.getContent() == null || jokeEntity.getContent().length() == 0){
			throw new Exception("content 不能为空");
		}
		
		//查询是否已经抓取过
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("sourceId", sourceId);
		properties.put("sourceType", sourceType);
		List<JokeEntity> jokeEntities = this.jokeDao.findByProperty(properties);
		if(jokeEntities != null && !jokeEntities.isEmpty()){//抓取过的返回
			logger.info("---------->joke is repeat，jokeId is {},sourceType is {}",sourceId,sourceType);
			return false;
		}else{//没抓取过的存入数据库
			jokeEntity.setJokeId(SequenceHelp.getNextSeqWithDate(SequenceName.JOKE_ID.getValue()));
			jokeEntity.setStatus(JokeStatus.INACTIVE);//新抓取的统一设置为 未激活
			jokeEntity.setCreateDate(new Date());
			jokeEntity.setContent(this.filterContent(jokeEntity.getContent()));
			ObjectId objectId = this.jokeDao.create(jokeEntity);
			
			//抓取的时候直接过滤和校验关键字
			if(this.checkContentKeyWord(jokeEntity.getContent())//存在关键字 
					|| this.checkContentRepeat(jokeEntity.getContent())//内容出现重复
					|| jokeEntity.getHistoryUpCount() < jokeEntity.getHistoryDownCount()){//down次数大于up次数
				//检验不通过
				jokeEntity = this.jokeDao.getById(objectId);
				jokeEntity.setStatus(JokeStatus.DEL);
				this.jokeDao.mergeEntity(jokeEntity);
			}else{
				jokeEntity = this.jokeDao.getById(objectId);
				jokeEntity.setStatus(JokeStatus.NORMAL);
				this.jokeDao.mergeEntity(jokeEntity);
			}
			return true;
		}
		
	}

	@Override
	public PagingResult<JokeEntity> queryJokeByPaging(String id, int page, int size){
//		List<JokeDTO> jokeDTOs = new ArrayList<>();
		
		int pOffset = (page-1)*size;
		Paging paging = new Paging();
		paging.setOffset(pOffset);
		paging.setLimit(size);
//		List<JokeEntity> jokeEntities = this.jokeDao.findByProperty(paging);
		
		/*for(JokeEntity temp:jokeEntities){
			jokeDTOs.add(JokeDTO.createJokeDto(temp));
		}*/
		
//		return jokeEntities;
		
		Map<String, Object> properites = new HashMap<>();
		properites.put("status", JokeStatus.NORMAL);
		return this.jokeDao.getPagingResult(properites, paging,"-jokeId");
	}
	
	@Override
	public List<JokeEntity> queryJokesForApp(String s, int size) {
		
		Map<String, Object> properites = new HashMap<>();
		properites.put("status", JokeStatus.NORMAL);
		
		if("0".equals(s)){//获取最新的jokes
			properites.put("jokeId <", "99999999999999");
			
			Paging paging = new Paging();
			paging.setLimit(size);
			paging.setOffset(0);
			return this.jokeDao.findByProperty(properites,paging,"-jokeId");
		}else{
			properites.put("jokeId <", s);
			
			Paging paging = new Paging();
			paging.setLimit(size);
			paging.setOffset(0);
			return this.jokeDao.findByProperty(properites,paging,"-jokeId");
		}
	}

	@Override
	public Boolean checkContentRepeat(String content) {
		//计算校验开始时间，以当前时间为基准
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH)-CommonConstants.CHECK_REPEAT_SCOPE);
		Date date = calendar.getTime();
		
		//获取所有符合条件的数据
		Map<String, Object> properties = new HashMap<>();
		properties.put("status", JokeStatus.NORMAL);
		properties.put("createDate >=", date);
		List<JokeEntity> jokeEntities = this.jokeDao.findByProperty(properties);
		
		//对比
		String targetStr = content;
		for(JokeEntity temp : jokeEntities){
			float similar = StringUtil.getSimilarityRatio(temp.getContent(), targetStr);
			if(similar > CommonConstants.SIMILAR_VALUE){//出现重复的返回true
				logger.debug("----->内容重复：{}",content);
				return true;
			}
		}
		
		return false;
	}

	@Override
	public Boolean checkContentKeyWord(String content){
		content = content.trim();
		
		WebConfigEntity webConfigEntity = this.webConfigDao.findOneByProperty("key", WebConfigKey.CHECK_KEY_WORD.name());
		if(webConfigEntity != null){
			String[] keyWords = webConfigEntity.getValue().split("\\|");
			for(String temp : keyWords){
				if(!"".equals(temp)){
					/**
					 * 1.%号作为like语法进行模糊匹配
					 * 2.其他则直接进行匹配
					 */
					if(-1 == temp.indexOf("%")){//对于不存在%号的 直接索引关键字
						if(-1 != content.indexOf(temp)){
							logger.debug("----->关键字校验：{}---->内容：{}",temp,content);
							return true;
						}
					}else{//存在%号的，执行like查询（使用正则表达式实现）
						String regex = temp.replace("%", "[\\S|\\s]+");
						if(Pattern.matches(regex, content)){
							logger.debug("----->关键字校验：{}---->内容：{}",temp,content);
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public String filterContent(String content) {
		content = content.trim();
		WebConfigEntity webConfigEntity = this.webConfigDao.findOneByProperty("key", WebConfigKey.FILTER_KEY_WORD.name());
		if(webConfigEntity != null){
			String[] keyWords = webConfigEntity.getValue().split("\\|");
			for(String temp : keyWords){
				content = content.replaceAll(temp, "");
			}
		}
		return content;
	}

	@Override
	public void up(String jokeId,int count) {
		UpdateOperations<JokeEntity> updateOperations = this.jokeDao.createUpdateOptions();
		updateOperations.inc("upCount", count);

		Map<String, Object> properties = new HashMap<>();
		properties.put("jokeId", jokeId);
		Query<JokeEntity> query = this.jokeDao.buildQuery(properties);
		
//		JokeEntity jokeEntity = new JokeEntity();
//		jokeEntity.setJokeId(jokeId);
		this.jokeDao.updateEntity(query, updateOperations);
	}

	@Override
	public void down(String jokeId,int count) {
		UpdateOperations<JokeEntity> updateOperations = this.jokeDao.createUpdateOptions();
		updateOperations.inc("downCount", 0-count);
		
		Map<String, Object> properties = new HashMap<>();
		properties.put("jokeId", jokeId);
		Query<JokeEntity> query = this.jokeDao.buildQuery(properties);

		this.jokeDao.updateEntity(query, updateOperations);
	}
}
