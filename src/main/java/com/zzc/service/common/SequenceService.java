package com.zzc.service.common;

import javax.annotation.Resource;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.zzc.common.tool.ISequenceService;
import com.zzc.db.mongo.dao.ISequenceDao;
import com.zzc.db.mongo.entity.SequenceEntity;

@Service("sequenceService")
public class SequenceService implements ISequenceService {
	private final long DEFAULT_SEQ_MAX_VALUE = 999999;//默认序列最大值
	private final long DEFAULT_SEQ_INIT_VALUE = 1;//默认序列初始值
	
	@Resource
	private ISequenceDao sequenceDao;
	
	@Override
	public long getNextSequence(String sequenceName, int allotment) {
		SequenceEntity sequenceEntity = this.sequenceDao.findOneByProperty("seqName", sequenceName);
		ObjectId objectId = null;
		if(sequenceEntity == null){
			objectId = this.createSeq(sequenceName);
			sequenceEntity = this.sequenceDao.findOneByProperty("id", objectId);
		}
		
		long currentValue = sequenceEntity.getCurrentValue();
		
		long currentValueNew;
		if(sequenceEntity.getCurrentValue()+allotment > sequenceEntity.getMaxValue()){
			currentValueNew = (sequenceEntity.getCurrentValue()+allotment)%sequenceEntity.getMaxValue();
		}else{
			currentValueNew = sequenceEntity.getCurrentValue()+allotment;
		}
		
		
		sequenceEntity.setCurrentValue(currentValueNew);
		
//		UpdateOperations<SequenceEntity> updateOperations = this.sequenceDao.createUpdateOptions().set("currentValue", currentValue);
//		this.sequenceDao.updateEntity(sequenceEntity, updateOperations);
		this.sequenceDao.mergeEntity(sequenceEntity);
		
		return currentValue;
	}

	@Override
	public long getSeqMaxValue(String sequenceName){
		SequenceEntity sequenceEntity = this.sequenceDao.findOneByProperty("seqName", sequenceName);
		ObjectId objectId = null;
		if(sequenceEntity == null){
			objectId = this.createSeq(sequenceName);
			sequenceEntity = this.sequenceDao.findOneByProperty("id", objectId);
		}
		return sequenceEntity.getMaxValue();
	}
	
	/**
	 * 创建序列
	 * @param seqName
	 * @return
	 */
	private ObjectId createSeq(String seqName){
		SequenceEntity entity = new SequenceEntity();
		entity.setSeqName(seqName);
		entity.setCurrentValue(this.DEFAULT_SEQ_INIT_VALUE);
		entity.setMaxValue(this.DEFAULT_SEQ_MAX_VALUE);
		return this.sequenceDao.create(entity);
	}

}
