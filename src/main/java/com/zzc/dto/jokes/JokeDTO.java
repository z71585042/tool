package com.zzc.dto.jokes;

import com.zzc.db.mongo.entity.JokeEntity;

public class JokeDTO {
	private String jokeId;
	private String content;
	private int upCount;
	private int downCount;
	
	public static JokeDTO createJokeDto(JokeEntity jokeEntity){
		JokeDTO jokeDTO = new JokeDTO();
		jokeDTO.setJokeId(jokeEntity.getJokeId());
		jokeDTO.setContent(jokeEntity.getContent());
		jokeDTO.setUpCount(jokeEntity.getUpCount());
		jokeDTO.setDownCount(jokeEntity.getDownCount());
		return jokeDTO;
	}
	
	public String getJokeId() {
		return jokeId;
	}
	public void setJokeId(String jokeId) {
		this.jokeId = jokeId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getUpCount() {
		return upCount;
	}
	public void setUpCount(int upCount) {
		this.upCount = upCount;
	}
	public int getDownCount() {
		return downCount;
	}
	public void setDownCount(int downCount) {
		this.downCount = downCount;
	}
	
}
