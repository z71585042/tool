package com.zzc.db.mongo.entity;

import java.util.Date;

import com.github.jmkgreen.morphia.annotations.Index;
import com.github.jmkgreen.morphia.annotations.Indexed;
import com.github.jmkgreen.morphia.annotations.Indexes;
import com.github.jmkgreen.morphia.utils.IndexDirection;
import com.zzc.common.CommonConstants.JokeSourceType;
import com.zzc.common.CommonConstants.JokeStatus;
import com.zzc.db.mongo.morphia.base.entity.AbstractMorphiaEntity;

/**
 * 段子
 * @author zhengzhichao
 *
 */
@Indexes(value = {@Index("status,-jokeId")})
public class JokeEntity extends AbstractMorphiaEntity {
	/**
	 * jokeId
	 */
	@Indexed(value = IndexDirection.DESC,unique = true)
	private String jokeId;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * 状态
	 */
	private JokeStatus status;
	/**
	 * up的次数
	 */
	private int upCount;
	
	/**
	 * down次数
	 */
	private int downCount;
	
	/**
	 * 历史 up的次数
	 */
	private int historyUpCount;
	
	/**
	 * 历史 down的次数
	 */
	private int historyDownCount;
	
	/**
	 * 来源
	 */
	private JokeSourceType sourceType;
	
	/**
	 * 来源id
	 */
	private String sourceId;
	/**
	 * 创建的时间
	 */
	private Date createDate;

	
	//set get
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getHistoryUpCount() {
		return historyUpCount;
	}

	public void setHistoryUpCount(int historyUpCount) {
		this.historyUpCount = historyUpCount;
	}

	public int getHistoryDownCount() {
		return historyDownCount;
	}

	public void setHistoryDownCount(int historyDownCount) {
		this.historyDownCount = historyDownCount;
	}

	public JokeSourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(JokeSourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public int getUpCount() {
		return upCount;
	}

	public void setUpCount(int upCount) {
		this.upCount = upCount;
	}

	public int getDownCount() {
		return downCount;
	}

	public void setDownCount(int downCount) {
		this.downCount = downCount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public JokeStatus getStatus() {
		return status;
	}

	public void setStatus(JokeStatus status) {
		this.status = status;
	}

	public String getJokeId() {
		return jokeId;
	}

	public void setJokeId(String jokeId) {
		this.jokeId = jokeId;
	}
}
