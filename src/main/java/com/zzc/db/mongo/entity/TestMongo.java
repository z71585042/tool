package com.zzc.db.mongo.entity;

import com.zzc.db.mongo.morphia.base.entity.AbstractMorphiaEntity;




/**
 * B2G系统配置相关
 * @author zhengzhichao
 *
 */
public class TestMongo extends AbstractMorphiaEntity{
	private String key;
	private String value;
	private String detail;//描述
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
}
