package com.zzc.db.mongo.entity;

import com.github.jmkgreen.morphia.annotations.Indexed;
import com.github.jmkgreen.morphia.utils.IndexDirection;
import com.zzc.db.mongo.morphia.base.entity.AbstractMorphiaEntity;

/**
 * 序列
 * @author zhengzhichao
 *
 */
public class SequenceEntity extends AbstractMorphiaEntity {
	/**
	 * 序列名字
	 */
	@Indexed(value=IndexDirection.ASC,unique=true)
	private String seqName;
	/**
	 * 序列当前值
	 */
	private long currentValue;
	/**
	 * 序列最大值
	 */
	private long maxValue;
	
	public String getSeqName() {
		return seqName;
	}
	public void setSeqName(String seqName) {
		this.seqName = seqName;
	}
	public long getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(long currentValue) {
		this.currentValue = currentValue;
	}
	public long getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(long maxValue) {
		this.maxValue = maxValue;
	}
}
