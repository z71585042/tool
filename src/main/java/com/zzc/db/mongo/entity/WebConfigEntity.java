package com.zzc.db.mongo.entity;

import java.util.Date;

import com.zzc.db.mongo.morphia.base.entity.AbstractMorphiaEntity;

public class WebConfigEntity extends AbstractMorphiaEntity {
	private String key;
	private String value;
	private Date updateDate;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
