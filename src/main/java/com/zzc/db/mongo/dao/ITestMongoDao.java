package com.zzc.db.mongo.dao;

import org.bson.types.ObjectId;

import com.zzc.db.mongo.entity.TestMongo;
import com.zzc.db.mongo.morphia.base.dao.ICoreMongoBaseDao;

public interface ITestMongoDao extends ICoreMongoBaseDao<TestMongo, ObjectId>{

}
