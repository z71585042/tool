package com.zzc.db.mongo.dao.impl;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import com.zzc.db.mongo.dao.IJokeDao;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.db.mongo.morphia.base.dao.impl.CoreMongoBaseDao;

@Repository
public class JokeDao extends CoreMongoBaseDao<JokeEntity, ObjectId> implements IJokeDao{

}
