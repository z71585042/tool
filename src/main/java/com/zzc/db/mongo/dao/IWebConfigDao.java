package com.zzc.db.mongo.dao;

import org.bson.types.ObjectId;

import com.zzc.db.mongo.entity.WebConfigEntity;
import com.zzc.db.mongo.morphia.base.dao.ICoreMongoBaseDao;

public interface IWebConfigDao extends ICoreMongoBaseDao<WebConfigEntity, ObjectId> {

}
