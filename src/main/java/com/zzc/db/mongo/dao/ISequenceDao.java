package com.zzc.db.mongo.dao;

import org.bson.types.ObjectId;

import com.zzc.db.mongo.entity.SequenceEntity;
import com.zzc.db.mongo.morphia.base.dao.ICoreMongoBaseDao;

public interface ISequenceDao extends ICoreMongoBaseDao<SequenceEntity, ObjectId> {

}
