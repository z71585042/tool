package com.zzc.db.mongo.dao.impl;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import com.zzc.db.mongo.dao.ISequenceDao;
import com.zzc.db.mongo.entity.SequenceEntity;
import com.zzc.db.mongo.morphia.base.dao.impl.CoreMongoBaseDao;

@Repository
public class SequenceDao extends CoreMongoBaseDao<SequenceEntity, ObjectId> implements ISequenceDao {

}
