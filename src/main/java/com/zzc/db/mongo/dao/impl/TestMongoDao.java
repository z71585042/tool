package com.zzc.db.mongo.dao.impl;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import com.zzc.db.mongo.dao.ITestMongoDao;
import com.zzc.db.mongo.entity.TestMongo;
import com.zzc.db.mongo.morphia.base.dao.impl.CoreMongoBaseDao;

@Repository("testMongoDao")
public class TestMongoDao extends CoreMongoBaseDao<TestMongo, ObjectId> implements ITestMongoDao{

}
