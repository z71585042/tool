package com.zzc.db.mongo.dao.impl;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;

import com.zzc.db.mongo.dao.IWebConfigDao;
import com.zzc.db.mongo.entity.WebConfigEntity;
import com.zzc.db.mongo.morphia.base.dao.impl.CoreMongoBaseDao;

@Repository
public class WebConfigDao extends CoreMongoBaseDao<WebConfigEntity, ObjectId> implements IWebConfigDao {

}
