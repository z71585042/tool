package com.zzc.core.spider;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import net.sf.json.JSONObject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Selector;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzc.core.RequestMethod;
import com.zzc.core.SimpleHttpRequest;


/**
 * 蜘蛛
 * @author zhengzhichao
 *
 */
public abstract class SimpleAbstractSpider implements Job{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static ExecutorService executorService = Executors.newFixedThreadPool(10);
	
	/**
	 * 定义失败次数，如果失败次数超过此值，则爬取中断
	 */
	private final int maxTryTimes = 5;
	
	/**
	 * 基础url
	 */
	private String baseUrl;
	
	/**
	 * 需要继续解析的a标签（链接） css选择器{@link Selector} 类似jquery的选择器即可
	 */
	private String aEleSelector;
	
	/**
	 * 解析的目标元素
	 * key 字段名称
	 * value selector（类似jquery的选择器）{@link Selector}
	 * 比如需要获取文章标题，表示为 title:div.title
	 */
	private Map<String,String> parseElements;
	
	/**
	 * 目标url
	 */
	private Queue<String> targetUrls = new LinkedList<>();
	/**
	 * 历史url
	 */
	private Set<String> historyUrls = new HashSet<>();
	
	
	/**
	 * 添加一个目标url
	 * @param url
	 */
	public void addTargetUrl(String url){
		this.targetUrls.add(url);
	}
	
	/**
	 * 获取目标块 需要分析html元素集合，比如div块
	 * @param doc
	 * @return
	 */
	public abstract Elements getTargetElements(Document doc);
	
	/**
	 * 处理抓取的数据，对于出现重复数据则返回false
	 * @param data 此参数内容与设置的parseElements有关系，他们的key是相同的，根据parseElements设置的属性名称和规则，获取解析后的属性名称和内容
	 * @return 返回true表示处理成功 返回false表示出现重复数据
	 */
	public abstract Boolean processData(Map<String, String> data) throws Exception;

	
	/**
	 * 执行
	 */
	public final void execute(JobExecutionContext context) throws JobExecutionException {
		logger.debug("---------------------->SimpleSpider execute begin");
		try{
			this.check();
			
			int tryTimes = 0;
			
			//发送请求获取页面内容
			while(!this.targetUrls.isEmpty()){
				String targetUrl = this.targetUrls.poll();//获取目标url
				this.historyUrls.add(targetUrl);//放到历史中
				
				/**
				 * 获取模版
				 */
				Map<String, String> requestHead = new HashMap<String, String>();
				requestHead.put("user-agent", "Mozilla/5.0 (Windows NT 6.1; rv:2.0b11) Gecko/20100101 Firefox/4.0b11");
				SimpleHttpRequest simpleHttpRequest = new SimpleHttpRequest(targetUrl,RequestMethod.GET,null,requestHead);
				Future<String> future = executorService.submit(simpleHttpRequest);
				
				String htmlStr = "";
				htmlStr = future.get();
				
				/**
				 * 解析页面
				 */
				Document doc = Jsoup.parse(htmlStr, this.getBaseUrl());
				this.getNextUrl(doc);//需要继续处理的url
				Elements elements = this.getTargetElements(doc);
				
				Map<String, String> data = new HashMap<>();
				for(Element ele : elements){//目标块div
//					logger.debug("---------------------->ele:{}",ele.html());
					Iterator<Entry<String, String>> it =  this.parseElements.entrySet().iterator();
					while(it.hasNext()){
						Entry<String, String> temp = it.next();
						
						String[] values = temp.getValue().split("@");
						if(values.length == 1){
							data.put(temp.getKey(), ele.select(values[0]).get(0).text());
						}else if(values.length == 2){//出现@符号，表示取当前元素的属性，@后的字段表示属性名称
							data.put(temp.getKey(), ele.select(values[0]).get(0).attr(values[1]));
						}else{
							throw new Exception("无法匹配的元素类型,key:"+temp.getKey()+",value:"+temp.getValue());
						}
						
					}
					
					logger.info("---------------------->data:{}",JSONObject.fromObject(data).toString());
					
					if(!this.processData(data)){//处理数据
						tryTimes++;
						logger.info("---------------------->process failed times:{}",tryTimes);
					}
					
					if(tryTimes > maxTryTimes){//处理失败
						logger.info("---------------------->process failed,break!");
						this.targetUrls.clear();//清空目标url，不再爬取
						break;//中断循环
					}
				}
			}
			logger.debug("---------------------->SimpleSpider execute end");
		}catch(Exception e){
			logger.error(e.getMessage(),e);
			throw new JobExecutionException(e.getMessage());
		}
	}
	
	/**
	 * 获取nextUrl
	 * @param doc
	 */
	public final void getNextUrl(Document doc){
		Elements eles = doc.select(this.aEleSelector);
		logger.info("---------------------->nextUrl elements is:"+eles.toString());
		for(Element ele : eles){
			String url = ele.absUrl("href");
			if(this.historyUrls.contains(url)){//判断是否出现过
				logger.info("---------------------->nextUrl is repead:{}",url);
			}else{
				this.targetUrls.add(url);
				logger.info("---------------------->nextUrl add success:{}",url);
			}
		}
	}
	
	/**
	 * 校验
	 */
	public final void check() throws Exception{
		if(this.baseUrl == null || this.baseUrl.length() == 0){
			throw new Exception("baseUrl不能为空");
		}
		
		if(this.parseElements == null || this.parseElements.size() == 0){
			throw new Exception("解析元素为空");
		}
		
		if(this.targetUrls.size() == 0){
			throw new Exception("没有目标url");
		}
		
		if(this.aEleSelector == null){
			this.aEleSelector = "";
		}
		
		logger.debug("---------------------->baseUrl:{}",this.baseUrl);
		logger.debug("---------------------->aEleSelector:{}",this.aEleSelector);
		logger.debug("---------------------->parseElements:{}",this.parseElements);
	}

	//setter getter
	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getaEleSelector() {
		return aEleSelector;
	}

	public void setaEleSelector(String aEleSelector) {
		this.aEleSelector = aEleSelector;
	}

	public Map<String, String> getParseElements() {
		return parseElements;
	}

	public void setParseElements(Map<String, String> parseElements) {
		this.parseElements = parseElements;
	}
}
