package com.zzc.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzc.common.CommonConstants.JokeSourceType;
import com.zzc.common.CommonConstants.JokeStatus;
import com.zzc.common.CommonConstants.SequenceName;
import com.zzc.common.tool.SequenceHelp;
import com.zzc.db.mongo.dao.IJokeDao;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.service.jokes.IJokeService;

@Controller
public class TestController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Resource
	private IJokeService jokeService;
	@Resource
	private IJokeDao jokeDao;
	
	/**
	 * 刷新所有已获取数据的时间 和 jokeId
	 * @return
	 */
//	@RequestMapping(value="test")
//	@ResponseBody
	public String test(){
		List<JokeEntity> jokeEntities = this.jokeDao.findAll();
		for(JokeEntity temp : jokeEntities){
			temp.setCreateDate(new Date());
			temp.setJokeId(SequenceHelp.getNextSeqWithDate(SequenceName.JOKE_ID.getValue()));
			
			if(JokeSourceType.BDJ == temp.getSourceType()){
				String[] strs = temp.getSourceId().split("-");
				temp.setSourceId(strs[strs.length-1]);
			}else if(JokeSourceType.QB == temp.getSourceType()){
				String[] strs = temp.getSourceId().split("_");
				temp.setSourceId(strs[strs.length-1]);
			}else{
				
			}
			
			this.jokeDao.mergeEntity(temp);
		}
		return "success";
	}
	
	/**
	 * 过滤关键字
	 * @return
	 */
	@RequestMapping(value="filterKeyWord")
	@ResponseBody
	public String filterKeyWord(){
		List<JokeEntity> jokeEntities = this.jokeDao.findByProperty("status !=", JokeStatus.DEL.name());
		for(JokeEntity temp : jokeEntities){
			
			//校验关键字
			if(this.jokeService.checkContentKeyWord(temp.getContent())){
				temp.setStatus(JokeStatus.DEL);
			}else{
				temp.setStatus(JokeStatus.NORMAL);
			}
			
			//过滤替换关键字
			temp.setContent(this.jokeService.filterContent(temp.getContent()));
			
			this.jokeDao.mergeEntity(temp);
		}
		
		return "success";
	}
}
