package com.zzc.controller.jokes;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.zzc.common.tool.AjaxResponse;
import com.zzc.common.tool.AjaxResponse.AjaxResponseStatus;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.service.jokes.IJokeService;

@Controller
@RequestMapping(value="app")
public class AppController {
	private final int DEFAULT_NUM = 20;
	@Resource
	private IJokeService jokeService;
	
	/**
	 * 获取更多
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/getMoreJokes")
	@ResponseBody
	public AjaxResponse getMoreJokes(WebRequest request){
		AjaxResponse ajaxResponse = new AjaxResponse();

		String s = request.getParameter("s") != null ? request.getParameter("s") : "0";
		
		List<JokeEntity> jokeEntities = this.jokeService.queryJokesForApp(s, this.DEFAULT_NUM);
		
		ajaxResponse.setData(jokeEntities);
		ajaxResponse.setStatus(AjaxResponseStatus.SUCCESS);
		
		return ajaxResponse;
	}
	
	/**
	 * 获取最新
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/getNewestJokes")
	@ResponseBody
	public AjaxResponse getNewestJokes(WebRequest request){
		AjaxResponse ajaxResponse = new AjaxResponse();
		
		List<JokeEntity> jokeEntities = this.jokeService.queryJokesForApp("0", this.DEFAULT_NUM);
		
		ajaxResponse.setData(jokeEntities);
		ajaxResponse.setStatus(AjaxResponseStatus.SUCCESS);
		return ajaxResponse;
	}
	
	/**
	 * up
	 * @return
	 */
	@RequestMapping(value="/up")
	@ResponseBody
	public AjaxResponse up(WebRequest request){
		AjaxResponse ajaxResponse = new AjaxResponse();
		String jokeId = request.getParameter("jokeId") != null ? request.getParameter("jokeId") : "";
		String count = request.getParameter("count") != null ? request.getParameter("count") : "1";//没有表示顶一次
		this.jokeService.up(jokeId,Integer.valueOf(count));
		
		ajaxResponse.setStatus(AjaxResponseStatus.SUCCESS);
		return ajaxResponse;
	}
	
	/**
	 * down
	 * @return
	 */
	@RequestMapping(value="/down")
	@ResponseBody
	public AjaxResponse down(WebRequest request){
		AjaxResponse ajaxResponse = new AjaxResponse();
		String jokeId = request.getParameter("jokeId") != null ? request.getParameter("jokeId") : "";
		String count = request.getParameter("count") != null ? request.getParameter("count") : "1";//没有表示down一次
		this.jokeService.down(jokeId,Integer.valueOf(count));
		
		ajaxResponse.setStatus(AjaxResponseStatus.SUCCESS);
		return ajaxResponse;
	}
}
