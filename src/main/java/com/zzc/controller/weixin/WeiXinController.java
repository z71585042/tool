package com.zzc.controller.weixin;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzc.common.weixin.ReplayTextMsg;
import com.zzc.service.weixin.WeixinService;


@Controller
@RequestMapping("/weixin")
public class WeiXinController {
	private final Logger logger = LoggerFactory.getLogger(WeiXinController.class);
	
	@Resource
	private WeixinService weixinService;
	/**
	 * 微信接口，供微信平台调用
	 * @param request
	 * @return
	 */
	@RequestMapping("pubItf")
	@ResponseBody
	public String pubItf(HttpServletRequest request){
		//参数列表，用来调试
		Map<String,String[]> param = request.getParameterMap();
		logger.debug("----->获取到的参数列表begin");
		for(Entry<String, String[]> temp:param.entrySet()){
			logger.debug(temp.getKey()+":"+temp.getValue().toString());
		}
		logger.debug("----->获取到的参数列表end");
		
		//准备参数
//		String token  = request.getParameter("token");
		String token = "zhengzc";
		String timestamp = request.getParameter("timestamp");
		String nonce = request.getParameter("nonce");
		String signature = request.getParameter("signature");
		String echostr = request.getParameter("echostr");
		
		logger.debug("----->param get form weixin:timestamp:"+timestamp+",nonce:"+nonce+",signature:"+signature+",echostr:"+echostr);
		
		if(this.checkSignature(token, timestamp, nonce, signature)){
			logger.debug("check is from weixin success!!");
			if(echostr!=null){
				return echostr;
			}else{
				//获取post过来的xml信息
				String fromWeixinMessage = this.processRequest(request);
				logger.debug("----->xml message from weixin:"+fromWeixinMessage);
				//处理消息
				XMLSerializer xmlSerializer = new XMLSerializer();
				JSONObject message = (JSONObject)xmlSerializer.read(fromWeixinMessage);
				String replayContent = "";
				if(message.optString("MsgType").equals("text")){//文本消息
					replayContent = this.weixinService.chat(message.optString("Content"));
				}else{//非文本消息
					replayContent = "哎呦，俺只是个小盆友，只能看懂文字哦！";
				}
				ReplayTextMsg replayTextMsg = new ReplayTextMsg(message.optString("FromUserName"), message.optString("ToUserName"), replayContent);
				logger.debug("relpay xml to weixin:"+replayTextMsg.toString());
				return replayTextMsg.toString();
			}
		}else{
			return "";
		}
	}
	
	/**
	 * 微信验证
	 * @param token
	 * @param timestamp
	 * @param nonce
	 * @return
	 */
	public Boolean checkSignature(String token,String timestamp,String nonce,String signature){
		String[] arr = {token,timestamp,nonce};
		Arrays.sort(arr);
		
		try {
			String str = arr[0]+arr[1]+arr[2];
			logger.debug("----->affter sort sting:"+str);
			
			byte[] temp = str.getBytes();
			
			String target = DigestUtils.shaHex(temp);
			
			logger.debug("----->after sha1 String:"+target+"----->checkResult:"+signature.equals(target));
			return signature.equals(target);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return false;
		}
	}
	
	/**
	 * 处理post请求发送过来的xml信息
	 * @param request
	 * @return
	 */
	public String processRequest(HttpServletRequest request){
		String xmlData="";
		
		// 取HTTP请求流  
        ServletInputStream inputStream;
		try {
			inputStream = request.getInputStream();
			 // 取HTTP请求流长度 
	        int size = request.getContentLength();  
	        // 用于缓存每次读取的数据  
	        byte[] buffer = new byte[size];  
	        // 用于存放结果的数组  
	        byte[] xmldataByte = new byte[size];  
	        int count = 0;  
	        int rbyte = 0;  
	        // 循环读取  
	        while (count < size) {   
	            // 每次实际读取长度存于rbyte中  
	            rbyte = inputStream.read(buffer);   
	            for(int i=0;i<rbyte;i++) {  
	                xmldataByte[count + i] = buffer[i];  
	            }  
	            count += rbyte;  
	        }  
	          
	        xmlData = new String(xmldataByte, "UTF-8"); 
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}  
        
		logger.debug("----->获取到的信息为："+xmlData);
		
		return xmlData;
	}
}
