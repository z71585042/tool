package com.zzc.controller;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.db.mongo.morphia.base.entity.PagingResult;
import com.zzc.service.jokes.IJokeService;

@Controller
public class MainController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final int PAGE_SIZE = 20;
	
	@Resource
	private IJokeService jokeService;
	
	@RequestMapping(value="new/page/{pageIndex}")
	public ModelAndView queryNewJoke(@PathVariable int pageIndex,WebRequest request){
		ModelAndView mv = new ModelAndView();
		
		List<JokeEntity> jokeEntities = null;
		long jokeCount = 0;
		try {
			PagingResult<JokeEntity> result = this.jokeService.queryJokeByPaging("", pageIndex, this.PAGE_SIZE);
			jokeCount = result.getTotalCount();
			jokeEntities = result.getResult();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		
		mv.getModel().put("pageIndex", pageIndex);
		mv.getModel().put("pageSize", this.PAGE_SIZE);
		mv.getModel().put("pageCount", jokeCount/PAGE_SIZE+1);
		mv.getModel().put("jokes", jokeEntities);
		
		mv.setViewName("page/newJoke.html");
		return mv;
	}
}
