package com.zzc.timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	private static final Logger logger = LoggerFactory.getLogger(App.class);
	
	public static void main(String[] args){
		try{
			logger.info("------>App is starting");
			new ClassPathXmlApplicationContext(new String[]{"spring/applicationContext.xml","spring/applicationContext-quartz.xml"});
			logger.info("------>App is completed");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
