package com.zzc.timer.jobs;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.quartz.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzc.common.CommonConstants.JokeSourceType;
import com.zzc.core.SpringAppContext;
import com.zzc.core.spider.SimpleAbstractSpider;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.service.jokes.IJokeService;

public class QbSpider extends SimpleAbstractSpider implements Job {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final JokeSourceType sourceType = JokeSourceType.QB;
	
	private IJokeService jokeService = SpringAppContext.getBean("jokeService");
	
	
	private int count = 0;
	private final int maxCount = 20;
	

	public QbSpider(){
		this.setBaseUrl("http://www.qiushibaike.com");
		this.setaEleSelector("html body div#content.main div.content-block div#content-left.col1 div.pagebar a.next");
		this.addTargetUrl("http://www.qiushibaike.com/week");
		
		Map<String, String> parseElements = new HashMap<String, String>();
		parseElements.put("content","div.content");
		parseElements.put("upCount","div ul li.up a span");
		parseElements.put("downCount", "div ul li.down a span");
		parseElements.put("sourceId", "div@id");
		
		this.setParseElements(parseElements);
	}
	
	@Override
	public Elements getTargetElements(Document doc) {
		return doc.select("html body div#content.main div.content-block div#content-left.col1 div.article")
				.not(":has(div.thumb)");//带图片的不要
	}

	@Override
	public Boolean processData(Map<String, String> data) throws Exception{
		
		JokeEntity jokeEntity = new JokeEntity();
		jokeEntity.setContent(data.get("content"));
		jokeEntity.setHistoryDownCount(Integer.valueOf(data.get("downCount")));
		jokeEntity.setHistoryUpCount(Integer.valueOf(data.get("upCount")));
		jokeEntity.setSourceType(this.sourceType);
		
//		sourceId需要特殊处理 只要id qiushi_tag_69699452
		String[] strs = data.get("sourceId").split("_");
		String sourceId = strs[strs.length-1];
		jokeEntity.setSourceId(sourceId);
		
		if(logger.isDebugEnabled() && count == maxCount){
			return false;
		}
		
		if(this.jokeService.checkAndAddJoke(jokeEntity)){
			logger.info("{} joke count is {}",this,sourceType,this.count);
			this.count++;
			return true;
		}else{
			return false;
		}
		
	}

}
