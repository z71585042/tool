package com.zzc.timer.jobs;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.quartz.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzc.common.CommonConstants.JokeSourceType;
import com.zzc.core.SpringAppContext;
import com.zzc.core.spider.SimpleAbstractSpider;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.service.jokes.IJokeService;

public class BDJSpider extends SimpleAbstractSpider implements Job {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final JokeSourceType sourceType = JokeSourceType.BDJ;
	
	private IJokeService jokeService = SpringAppContext.getBean("jokeService");
	
	
	private int count = 0;
	private final int maxCount = 20;


	public BDJSpider(){
		this.setBaseUrl("http://www.budejie.com");
		//                    html body div#main.min-height div#web_content div.web_content_left div#pages p.budejie_ye a
		this.setaEleSelector("html body div#main.min-height div#web_content div.web_content_left div#pages p.budejie_ye a:contains(下一页)");
		this.addTargetUrl("http://www.budejie.com/hotdoc/");
		
		Map<String, String> parseElements = new HashMap<String, String>();
		//div.white_border div.web_conter div.web_list_left p
		//div.white_border div.web_conter div.web_list_left p#title-4784478.web_size
		parseElements.put("content","div.white_border div.web_conter div.web_list_left p");
		//div.white_border div.web_conter div.budejie_mutual ul.floatl li a.no_love span
		parseElements.put("upCount","div.white_border div.web_conter div.budejie_mutual ul.floatl li a.no_love span");
		//div.white_border div.web_conter div.budejie_mutual ul.floatl li a.no_cai span
		parseElements.put("downCount", "div.white_border div.web_conter div.budejie_mutual ul.floatl li a.no_cai span");
		parseElements.put("sourceId", "div.white_border div.web_conter div.web_list_left p@id");
		
		this.setParseElements(parseElements);
	}
	
	@Override
	public Elements getTargetElements(Document doc) {
		return doc.select("html body div#main.min-height div#web_content div.web_content_left div.web_left");
	}

	@Override
	public Boolean processData(Map<String, String> data) throws Exception{
		
		JokeEntity jokeEntity = new JokeEntity();
		jokeEntity.setContent(data.get("content"));
		jokeEntity.setHistoryDownCount(Integer.valueOf(data.get("downCount")));
		jokeEntity.setHistoryUpCount(Integer.valueOf(data.get("upCount")));
		jokeEntity.setSourceType(this.sourceType);
		
		//sourceId需要处理一下 只要id(title-4758416)
		String[] strs = data.get("sourceId").split("-");
		String sourceId = strs[strs.length-1];
		jokeEntity.setSourceId(sourceId);
		
		if(logger.isDebugEnabled() && count == maxCount){
			return false;
		}
		
		if(this.jokeService.checkAndAddJoke(jokeEntity)){
			this.count++;
			logger.info("{} joke count is {}",this,sourceType,this.count);
			return true;
		}else{
			return false;
		}
		
	}

}
