package com.zzc.timer.jobs;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.quartz.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zzc.common.CommonConstants.JokeSourceType;
import com.zzc.core.SpringAppContext;
import com.zzc.core.spider.SimpleAbstractSpider;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.service.jokes.IJokeService;

public class NeiHanSpider extends SimpleAbstractSpider implements Job {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final JokeSourceType sourceType = JokeSourceType.NH;
	
	private IJokeService jokeService = SpringAppContext.getBean("jokeService");
	
	
	private int count = 0;
	private final int maxCount = 20;


	public NeiHanSpider(){
		this.setBaseUrl("http://www.neihanshequ.com");
		//html body div.container div#article div.pin_paginator a 
		this.setaEleSelector("html body div.container div#article div.pin_paginator a:contains(下一页>>)");
		this.addTargetUrl("http://www.neihanshequ.com/joke/");
		
		Map<String, String> parseElements = new HashMap<String, String>();
		//html body div.container div#article div.pin_list div.pin
		//html body div.container div#article div.pin_list div.pin div.pin-content div.pin-context a.image p
		parseElements.put("content","div.pin-content div.pin-context a.image p");
		//html body div.container div#article div.pin_list div.pin div.pin-content div.controller table tbody tr td span a.digg
		parseElements.put("upCount","div.pin-content div.controller table tbody tr td span a.digg");
		//html body div.container div#article div.pin_list div.pin div.pin-content div.controller table tbody tr td span a.bury
		parseElements.put("downCount", "div.pin-content div.controller table tbody tr td span a.bury");
		//html body div.container div#article div.pin_list div.pin div.pin-content
		parseElements.put("sourceId", "div.pin-content@group_id");
		
		this.setParseElements(parseElements);
	}
	
	@Override
	public Elements getTargetElements(Document doc) {
		return doc.select("html body div.container div#article div.pin_list div.pin");
	}

	@Override
	public Boolean processData(Map<String, String> data) throws Exception{
		
		JokeEntity jokeEntity = new JokeEntity();
		jokeEntity.setContent(data.get("content"));
		jokeEntity.setHistoryDownCount(Integer.valueOf(data.get("downCount")));
		jokeEntity.setHistoryUpCount(Integer.valueOf(data.get("upCount")));
		jokeEntity.setSourceType(this.sourceType);
		jokeEntity.setSourceId(data.get("sourceId"));
		
		//sourceId需要处理一下 只要id(title-4758416)
//		String[] strs = data.get("sourceId").split("-");
//		String sourceId = strs[strs.length-1];
//		jokeEntity.setSourceId(sourceId);
		
		if(logger.isDebugEnabled() && count == maxCount){
			return false;
		}
		
		if(this.jokeService.checkAndAddJoke(jokeEntity)){
			this.count++;
			logger.info("{} joke count is {}",this,sourceType,this.count);
			return true;
		}else{
			return false;
		}
		
	}

}
