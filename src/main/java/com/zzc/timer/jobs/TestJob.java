package com.zzc.timer.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.zzc.core.SpringAppContext;

public class TestJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		Object obj = SpringAppContext.getBean("weixinService");
		System.out.println("--------------->hello world!"+obj.toString());
	}

}
