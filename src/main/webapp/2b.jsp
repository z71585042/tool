<!DOCTYPE html>
<html lang="zh-CN"  >
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>welcome</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		
		<script type="text/javascript">
			$(function($){
				
				function randomNumFn(min,max){
				    return Math.floor(min+Math.random()*(max-min));
				}
				
				
				var runNum = "";
				//随机数
				function genRanNum(){
					var font = randomNumFn(1,999999) + "";
					
					var tempStr = revStr(font); 
					tempStr = tempStr.replace(/[^\d^\.]/g, '').replace(/(\d{3})(?=\d)/g, "$1 ");
					
					
					ranNum = revStr(tempStr)+"."+randomNumFn(1,99);
					$("#randomNum").text(ranNum);
				}
				
				//倒序
				function revStr(str){
					var temp = str;
					return temp.split("").reverse().join("")
				}
				
				var timeCounter;//计时
				
				//统计
				var all = 0;
				var courrect = 0;
				
				//定时器
				var timer = $.noop;
				function autoCycleFn(){
					timeCounter--;
					$("#nextTime").text(timeCounter);
					timer = setTimeout(function(){
						if(timeCounter > 0){
							autoCycleFn();
						}else{
							stop();
						}
						
// 						console.log(timeCounter);
					},1000*1);//每秒一次
				}
				
				function init(){
					stop();
					//清空统计信息
					all = 0;
					courrect = 0;
					
					genRanNum();//生成随机数
					$("#in").show();//显示
					$("#in").focus();//聚焦
					
					//启动定时器
					var setTimeMin = $("#set").val();
					if(isNaN(setTimeMin)){
						alert("PleaseentertheNumbers!");
						return false;
					}
					timeCounter = 60*setTimeMin;
					timeCounter = timeCounter + 1;
					autoCycleFn();
				}
				
				function stop(){
					$("#in").hide();//隐藏输入
					clearTimeout(timer);
					$("#msg").text("Submit "+all+" times,"+courrect+" success,accuracy of "+courrect/all);
				}
				
				
				$("#begin").bind("click",init);
				
				$("#end").bind("click",stop);
				
				$("#in").keyup(function (e) {
					var curKey = e.which;
					var flag = true;
					if(curKey == 13){//回车键 提交
						all++;//总提交次数
// 						console.log(all);
						if(this.value == ranNum){
							courrect++;//正确次数
// 							console.log(courrect);
							$("#msg").prop("style","color:blue");
							this.value = "";
							$("#msg").text("correct");
							genRanNum();
						}else{
							$("#msg").prop("style","color:red");
							$("#msg").text("error!!!!");
// 							console.log(courrect);
						}
					}else if(curKey == 110){//小数点
						if(flag){
							var tempStr = revStr(this.value.substring(1,this.value.length-1)); 
							tempStr = tempStr.replace(/[^\d^\.]/g, '').replace(/(\d{3})(?=\d)/g, "$1 ");
							flag = false;
						}else{
						}
					}else{
						var tempStr = revStr(this.value); 
						tempStr = tempStr.replace(/[^\d^\.]/g, '').replace(/(\d{3})(?=\d)/g, "$1 ");
						
	                	this.value = revStr(tempStr);
					}
					
	            })
	            
	            
			})
		</script>
	</head>
	<body>
		<input id="set" type="text" style="width: 20px;" value="1" />min
		<input id="begin" type="button" value="begin" />
		<input id="end" type="button" value="end" />
		<div id="msg"></div>
		<div id="nextTime"></div>
		<hr/>
		
		
		<div id="randomNum" style="color: blue;font-size: 40px;"></div>
		<input id="in" type="text" style="font-size: 40px;" value=""/>
		<!-- <table>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table> -->
	</body>
</html>