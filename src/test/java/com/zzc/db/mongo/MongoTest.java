package com.zzc.db.mongo;

import javax.annotation.Resource;

import org.junit.Test;

import com.zzc.BaseTest;
import com.zzc.common.CommonConstants.WebConfigKey;
import com.zzc.common.tool.ISequenceService;
import com.zzc.common.tool.SequenceHelp;
import com.zzc.db.mongo.dao.IJokeDao;
import com.zzc.db.mongo.dao.ITestMongoDao;
import com.zzc.db.mongo.dao.IWebConfigDao;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.db.mongo.entity.TestMongo;
import com.zzc.db.mongo.entity.WebConfigEntity;

public class MongoTest extends BaseTest{
	
	@Resource
	private ITestMongoDao testMongoDao; 
	@Resource
	private ISequenceService sequenceService;
	@Resource
	private IJokeDao jokeDao;
	@Resource
	private IWebConfigDao webConfigDao;
	
	@Test
	public void test(){
		TestMongo testMongo = new TestMongo();
		testMongo.setKey("test");
		testMongo.setValue("123123");
		testMongo.setDetail("test拉拉");
		testMongoDao.create(testMongo);
	}
	
	@Test
	public void test2(){
		JokeEntity jokeEntity = new JokeEntity();
		this.jokeDao.create(jokeEntity);
	}
	
	@Test
	public void test3(){
		WebConfigEntity webConfigEntity = new WebConfigEntity();
		webConfigEntity.setKey(WebConfigKey.CHECK_KEY_WORD.name());
		webConfigEntity.setValue("糗百|糗友");
		this.webConfigDao.create(webConfigEntity);
	}
	
	@Test
	public void testSeq() throws Exception{
		for(int i = 0;i < 100;i++){
			System.out.println(SequenceHelp.getNextSeq("testSeq"));
		}
	}
	
	@Test
	public void testSeqService() throws Exception{
		for(int i = 0;i < 100;i++){
			System.out.println(this.sequenceService.getNextSequence("testSeq",2));
		}
	}
}
