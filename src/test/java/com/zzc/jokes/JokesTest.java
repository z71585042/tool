package com.zzc.jokes;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import com.zzc.BaseTest;
import com.zzc.core.RequestMethod;
import com.zzc.core.SimpleHttpRequest;

public class JokesTest extends BaseTest {
	
	/**
	 * 默认竟然是移动端格式
	 */
	@Test
	public void test(){
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		SimpleHttpRequest simpleHttpRequest = new SimpleHttpRequest("http://www.qiushibaike.com/week",RequestMethod.GET);
		
		Future<String> future = executorService.submit(simpleHttpRequest);
		
		try {
			System.out.println(future.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test1(){
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		
		Map<String, String> requestParam = new HashMap<>();
		
		Map<String, String> requestHead = new HashMap<String, String>();
		requestHead.put("user-agent", "Mozilla/5.0 (Windows NT 6.1; rv:2.0b11) Gecko/20100101 Firefox/4.0b11");
		
		SimpleHttpRequest simpleHttpRequest = new SimpleHttpRequest("http://www.qiushibaike.com/week",RequestMethod.GET,requestParam,requestHead);
		
		Future<String> future = executorService.submit(simpleHttpRequest);
		
		try {
			System.out.println(future.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test2(){
		try {
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			
			/**
			 * 发送请求获取页面内容
			 */
			Map<String, String> requestParam = new HashMap<>();
			Map<String, String> requestHead = new HashMap<String, String>();
			requestHead.put("user-agent", "Mozilla/5.0 (Windows NT 6.1; rv:2.0b11) Gecko/20100101 Firefox/4.0b11");
			SimpleHttpRequest simpleHttpRequest = new SimpleHttpRequest("http://www.qiushibaike.com/week",RequestMethod.GET,requestParam,requestHead);
			Future<String> future = executorService.submit(simpleHttpRequest);
			String htmlStr = future.get();
			
			/**
			 * jsoup解析内容
			 * 
			 * html body div#content.main div.content-block div#content-left.col1 div#qiushi_tag_68877391.article
			 * div.thumb
			 * 
			 */
			Document doc = Jsoup.parse(htmlStr, "www.qiushibaike.com");
			Elements elements = doc.select("html body div#content.main div.content-block div#content-left.col1 div.article")
									.not(":has(div.thumb)");//带图片的不要
			for(Element ele : elements){//每条joke
				System.out.println("----------------------");
//				System.out.println(ele.html());
				
				System.out.println(ele.select("div.content").get(0).text());//div.content
				System.out.println(ele.select("div ul li.up a").get(0).text());//div ul li.up a
				System.out.println("----------------------");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test3(){
		String html = "<div id='qiushi_tag_68807884' class='article block untagged mb15 bs2'>"+
				""+
				"<!-- <div class='detail'>"+
				"<a href='/article/68807884?list=week&s=4658434' target='_blank'>糗事#68807884</a>"+
				"</div> -->"+
				""+
				""+
				"<div class='author'>"+
				"<a href='/users/14388572'><img alt='女人，乖巧点' src='http://static.qiushibaike.com/images/thumb/missing.png'></a>"+
				"<a href='/users/14388572'>女人，乖巧点 </a>"+
				"<a class='send-msg' href='/users/14388572#?to=14388572&amp;from=68807884&amp;login=女人，乖巧点&amp;icon=None'>发小纸条</a>"+
				"</div>"+
				""+
				"<div title='2014-04-14 08:34:26' class='content'>"+
				""+
				"能过吗"+
				""+
				"</div>"+
				""+
				"<div class='thumb'>"+
				""+
				"<a onclick='_hmt.push(['_trackEvent', 'post', 'click', 'signlePost'])' target='_blank' href='/article/68807884?list=week&amp;s=4658434'>"+
				"<img alt='糗事#68807884' src='http://pic.qiushibaike.com/system/pictures/6880/68807884/medium/app68807884.jpg'>"+
				"</a>"+
				""+
				"</div>"+
				""+
				""+
				"<div class='bar clearfix' id='qiushi_counts_68807884'>"+
				"<ul>"+
				"<li class='up' id='vote-up-68807884'>"+
				"<a title='32027个顶' id='up-68807884' href='javascript:vote2(68807884,1)'>32027</a>"+
				"</li>"+
				"<li class='down' id='vote-dn-68807884'>"+
				"<a title='-454个拍' id='dn-68807884' href='javascript:vote2(68807884,-1)'>-454</a>"+
				"</li>"+
				""+
				"<li class='comment'>"+
				"<a onclick='_hmt.push(['_trackEvent', 'post', 'click', 'signlePost'])' target='_blank' title='86条评论' class='qiushi_comments' id='c-68807884' href='/article/68807884?list=week&amp;s=4658434'>86</a>"+
				"</li>"+
				""+
				"<li class='share'>"+
				"<a href='javascript:void(0);'>分享</a>"+
				"</li>"+
				"</ul>"+
				"<div class='sharebox'>"+
				"<div data='' class='bdshare_t bds_tools get-codes-bdshare' id='bdshare'>"+
				"<a class='bds_renren' title='分享到人人网' href='#'>人人网</a>"+
				"<a class='bds_qzone' title='分享到QQ空间' href='#'>QQ空间</a>"+
				"<a class='bds_tsina' title='分享到新浪微博' href='#'>新浪微博</a>"+
				"<a class='bds_tqq' title='分享到腾讯微博' href='#'>腾讯微博</a>"+
				"<!-- <a class='bds_mshare'>一键分享</a> -->"+
				"<div class='arrow'></div>"+
				"</div>"+
				"</div>"+
				"</div>"+
				" "+
				"</div>";
		
		Document doc = Jsoup.parse(html);
		Elements elements = doc.select("div.article").not(":has(div.thumb)");
		for(Element ele : elements){
			if(ele.select("dev dev.thumb").size() > 0){
				
			}else{
				System.out.println(ele.html());
			}
		}
	}
}
