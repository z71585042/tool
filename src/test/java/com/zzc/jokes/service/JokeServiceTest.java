package com.zzc.jokes.service;

import javax.annotation.Resource;

import org.junit.Test;

import com.zzc.BaseTest;
import com.zzc.common.CommonConstants.JokeSourceType;
import com.zzc.db.mongo.entity.JokeEntity;
import com.zzc.service.jokes.IJokeService;

public class JokeServiceTest extends BaseTest{
	@Resource
	private IJokeService jokeService;
	
	@Test
	public void testCheckAndAddJoke(){
		JokeEntity jokeEntity = new JokeEntity();
		jokeEntity.setContent("一小伙装干净，一天老远发现一挑大粪的农夫。就开始捂着口鼻前行，到了靠近粪桶的时候实在憋不住了，开始大口喘气，农夫自语道；“还有这喜好！！");
		jokeEntity.setSourceId("323423423423");
		jokeEntity.setSourceType(JokeSourceType.BDJ);
		jokeEntity.setHistoryDownCount(0);
		jokeEntity.setHistoryUpCount(0);
		try {
			this.jokeService.checkAndAddJoke(jokeEntity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFilterKeyWord(){
		String test = this.jokeService.filterContent("一小伙装干净，一天老远发现一挑大粪的农夫。就开始捂着口鼻前行，到了靠近粪桶的时候实在憋不住了，开始大口喘气，农夫自语道；“还有这喜好！！@糗事百科");
		System.out.println(test);
	}
	
	@Test
	public void testUp(){
		this.jokeService.up("20140428000521",-2);
	}
	
	@Test
	public void testDown(){
		this.jokeService.down("20140428000521",-2);
	}
}
