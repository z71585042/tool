package com.zzc.service.weixin;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zzc.service.weixin.WeixinService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/applicationContext.xml"})
public class ChatRobotServiceTest {
	@Resource
	private WeixinService weixinService;
	
	@Test
	public void chatTest(){
		String ret = weixinService.chat("妹纸");
		System.out.println("----->"+ret);
	}
}
